
FROM ubuntu:focal
RUN apt-get -y update && apt-get -y install telnet
ENTRYPOINT ["/usr/bin/telnet"]
CMD []
